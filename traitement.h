#ifndef __TRAITEMENT__H__
#define __TRAITEMENT__H__

int fonctionSomme(int *tabDeVal, int taille);
int *fonctionInverse(int *tabDeVal, int taille);
int *fonctionMultRangImpaire(int *tabDeVal, int taille);

#endif