#include <stdio.h>
#include <stdlib.h>
#include "traitement.h"


void fonctionTraitement(){

    int taille = 5;
    int tabDeVal[5];
    for (int i = 0; i < 5; i++){
        tabDeVal[i] = i<<3;
    }

    for (int i = 0; i < 5; i++){
        printf("[%d]", tabDeVal[i]);
    }

    printf("\n\n");

    // Faire la somme de tout les nombres de le tableau
    int recupeSomme = fonctionSomme(tabDeVal, taille);
    printf("Somme du tableau : %d", recupeSomme);

    printf("\n\n"); // SEPARATION

    // Inverser les nombres du tableau
    int *tabInverse = fonctionInverse(tabDeVal, taille);

    printf("Tableau inversé : ");
    for (int i = 0; i < 5; i++){
        printf("[%d]", tabInverse[i]);
    }

    printf("\n\n"); // SEPARATION

    // Multipier par -2 les nombres au rang impaire
    int *tabMultRangImpaire = fonctionMultRangImpaire(tabDeVal, taille);

    printf("Tableau inversé : ");
    for (int i = 0; i < 5; i++){
        printf("[%d]", tabMultRangImpaire[i]);
    }

}


int main(void) {

    printf("\n");
    fonctionTraitement();
    printf("\n\n");
    
	return EXIT_SUCCESS;
}