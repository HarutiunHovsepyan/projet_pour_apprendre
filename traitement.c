#include <stdio.h>
#include <stdlib.h>
#include "traitement.h"

int fonctionSomme(int *tabDeVal, int taille){

    int somme = 0;
    for (int i = 0; i < taille; i++){
        somme += tabDeVal[i];
    }

    return somme;
}
int *fonctionInverse(int *tabDeVal, int taille){

    int tailleActuel = taille;
    int *tabInverse;
    tabInverse = calloc(taille, sizeof(int));

    for (int i = 0; i < taille; i++){
        tabInverse[i] = tabDeVal[tailleActuel-1];
        tailleActuel -= 1;
    }

    return tabInverse;
}
int *fonctionMultRangImpaire(int *tabDeVal, int taille){

    int *tab;
    tab = calloc(taille, sizeof(int));

    for (int i = 0; i < taille; i++){
        tab[i] = tabDeVal[i];
        if (i % 2 != 0){
            tab[i] *= -2;
        }
    }
    
    return tab;
}